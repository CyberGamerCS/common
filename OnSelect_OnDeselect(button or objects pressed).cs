using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyClass : MonoBehaviour, ISelectHandler, IDeselectHandler
{
    public void OnSelect(BaseEventData eventData)
    {
        Debug.Log("selected:" + eventData.selectedObject);
    }

    public void OnDeselect(BaseEventData eventData)
    {
        Debug.Log("deselected:" + eventData.selectedObject);
    }
}

